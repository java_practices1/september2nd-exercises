import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        System.out.println("This is a program to break an integer into a sequence of individual digits.");
        System.out.println("---------------------------------------------------------------------------");
        System.out.println("Please enter your ZIP Code: ");
        int number = scanner.nextInt();
        String stringNumber = Integer.toString(number);

        if (number > 0){
            System.out.println("Your ZIP Code is : ");

            for (int i = 0; stringNumber.length() > i; i++){
                System.out.print(stringNumber.charAt(i) + "\t");
            }
        }
        else {
            System.out.println("Invalid number");
        }
    }
}
