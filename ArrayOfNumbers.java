import java.util.Arrays;

public class Main {

    public static void main(String[] args) {
        int arrayOfNumbers [] = {20, 86, 297, 9734, 93, 1, 8};
        int size = arrayOfNumbers.length;
        Arrays.sort(arrayOfNumbers);
        int firstMax = arrayOfNumbers[size-1];
        int secondMax = arrayOfNumbers[size-2];
        int thirdMax = arrayOfNumbers[size-3];

        System.out.println("The 3 largest numbers contained in the array are the following: ");
        System.out.println(firstMax + "\n" + secondMax + "\n" + thirdMax);
    }
}
