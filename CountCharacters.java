public class Main {

    public static void main(String[] args) {

        System.out.println("This is a Java program that count all characters of an input String.");
        System.out.println("---------------------------------------------------------------------------");
        System.out.println();
        String characters = "A72 014 ** 24D";
        System.out.println("String given: " + characters);

        count(characters);
    }
    public static void count(String ch) {
        char[] x = ch.toCharArray();
        int digits = 0;
        int letters = 0;
        int spaces = 0;
        int otherCharacters = 0;


        for (int i = 0; i < ch.length(); i++) {
            if (Character.isLetter(x[i])) {
                letters++;
            } else if (Character.isDigit(x[i])) {
                digits++;
            } else if (Character.isSpaceChar(x[i])) {
                spaces++;
            } else {
                otherCharacters++;
            }
        }

        System.out.println("NUmber of letters: " + letters);
        System.out.println("Number of digits: " + digits);
        System.out.println("Number of spaces: " + spaces);
        System.out.println("Other characters: " + otherCharacters);
    }
}
