# September 2nd Exercises

**Solve the following exercises.**
1. Write a Java program to break an integer into a sequence of individual digits.

2. Write a Java program to count the letters, spaces, numbers and other characters of an input string.

3. Write a Java program to find the 3 largest elements in a given array. Elements in the array can be in any order.

*Expected Output:*  
Original Array:  
[1, 4, 17, 7, 25, 3, 100]  
3 largest elements of the array are:  
100 25 17
